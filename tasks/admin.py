from django.contrib import admin

# Register your models here.
from tasks.models import Task


@admin.register(Task)
class Task(admin.ModelAdmin):
    list_display = (
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
    )
