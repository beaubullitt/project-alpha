from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm


# Create your views here.


@login_required
def list_projects(request):
    project_data = Project.objects.filter(owner=request.user)
    context = {
        "project_data": project_data,
    }
    return render(request, "projects/list_projects.html", context)


def project_details(request, id):
    individual_project_details = Project.objects.get(id=id)
    task_details = individual_project_details.tasks.all()
    context = {
        "project_details": individual_project_details,
        "task_details": task_details,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
