from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from accounts.forms import LoginForm, SignupForm
from projects.views import list_projects
from django.contrib.auth import login, logout
from django.contrib.auth.models import User


# Create your views here.
def login_user(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                # ??? DO I NOT NEED BELOW LINE?
                login(request, user)
                return redirect(list_projects)
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect(login_user)


def signup(request):
    if request.method == "POST":
        signup_data = SignupForm(request.POST)
        if signup_data.is_valid():
            username = signup_data.cleaned_data["username"]
            password = signup_data.cleaned_data["password"]
            password_confirmation = signup_data.cleaned_data[
                "password_confirmation"
            ]

            if password == password_confirmation:
                user = User.objects.create_user(username, password)
                user.save()
                login(request, user)
                return redirect(list_projects)
            elif password != password_confirmation:
                signup_data.add_error("password", "Passwords do not match")
    else:
        signup_data = SignupForm(request.GET)
    context = {
        "form": signup_data,
    }
    return render(request, "accounts/signup.html", context)
